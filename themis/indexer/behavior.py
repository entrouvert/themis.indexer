from zope.interface import Interface


class IThemisFieldIndexer(Interface):
    """Dexterity behavior interface for enabling the dynamic SearchableText
    indexer on known Themis fields.
    """
